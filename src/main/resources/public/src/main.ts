import Vue from 'vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import './assets/styles/main.css'
import VueParticles from 'vue-particles'
Vue.use(VueParticles)
import App from './App.vue'
// @ts-ignore
window.$ = require('jquery')

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
