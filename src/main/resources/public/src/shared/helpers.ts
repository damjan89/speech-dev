export const AllMainFilesList = ['router', 'store', 'service', 'services', 'model', 'models']
export const NewComponentQuestions = [
  'Do you want to make this component shared?',
  'Specify where to save the component (click on desired folder into the project explorer):',
  'Specify the name of the component',
  'Do you want to include this component into the router?',
  'Do you want to generate a service for this component?',
  'Do you want to generate a model file for this component?']
export const Actions = [
  {value: "one", action: "component"}, //creates new component
  {value: "two", action: "service"}, //creates new service
  {value: "three", action: "model"}, //creates new service
]
