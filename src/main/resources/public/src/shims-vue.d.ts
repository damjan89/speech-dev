
declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module ' @hoducha/recorderjs' {
  import Recorder from ' @hoducha/recorderjs'
}
declare module 'inline-worker' {
  import Recorder from 'inline-worker'
}
declare module '@/shared/recorder.js' {
  import Recorder from '@/shared/recorder.js'
}
declare module '@/shared/helpers.js'
declare module 'vue-particles'
